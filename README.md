# Jira DOS SLA Tracker

## Login to JIRA
Remeber to add your Jira credentials to the JiraApi constructor in App.js
```
const jira = new JiraApi({
  protocol: 'http',
  host: 'supportdesk.building-blocks.com',
  username: 'username',
  password: 'password',
  apiVersion: '2',
  strictSSL: true
});
```
---


### Running the project

To run the project clone the repo and run 
``` 
npm install
npm start
```
--- 