import {
  GET_TICKETS_SLAS,
  ORDER_TICKETLIST,
  GET_JIRA_DATA,
  GET_TICKETSTIMETOSLA,
  UPDATE_CONFETTI
} from "../constants";
import JiraApi from "jira-client";
import slaConfig from "../../../lib/config/sla.config";
import moment from "moment";
import humanizeDuration from "humanize-duration";

export function getJiraData() {
  return function(dispatch) {
    const query = `category = "Support Contract" AND status in (open, triage, "clarifying request", "on hold")  and assignee = aholt AND created >= startOfYear() ORDER BY created DESC, key ASC`;
    const jira = new JiraApi({
      protocol: "http",
      host: "supportdesk.building-blocks.com",
      username: "lbrannagan",
      password: "cr8q$dEC",
      apiVersion: "2",
      strictSSL: true
    });

    return jira.searchJira(query).then(res => {
      const response = res.issues.map(ticket =>
        Object.assign(ticket, { ticketId: ticket.key })
      );
      return dispatch({ type: GET_JIRA_DATA, jiraTickets: response });
    });
  };
}

export function orderJiraTickets(jiraTickets) {
  const orderedTickets = jiraTickets.sort(
    (ticketA, ticketB) => moment(ticketA.diff) - moment(ticketB.diff)
  );
  return { type: ORDER_TICKETLIST, jiraTickets: orderedTickets };
}

export function getTicketsSlas(jiraTickets) {
  const updatedTicketList = [];
  jiraTickets.forEach(row => {
    slaConfig.forEach(({ level, time }) => {
      if (level === row.fields.priority.name) {
        Object.assign(row, { ticketSla: time });
        updatedTicketList.push(row);
      }
    });
  });
  return { type: GET_TICKETS_SLAS, jiraTickets: updatedTicketList };
}

export function getTimesToSla(jiraTickets) {
  const updatedTicketList = [];
  jiraTickets.forEach(row => {
    const today = new Date();

    const workingStart = new Date();
    workingStart.setFullYear(today.getFullYear());
    workingStart.setMonth(today.getMonth());
    workingStart.setDate(today.getDate());
    workingStart.setHours(8);
    workingStart.setMinutes(0);
    workingStart.setSeconds(0);
    workingStart.setMilliseconds(0);

    let workingEnd = new Date();
    workingEnd.setFullYear(today.getFullYear());
    workingEnd.setMonth(today.getMonth());
    workingEnd.setDate(today.getDate());
    workingEnd.setHours(17);
    workingEnd.setMinutes(30);
    workingEnd.setSeconds(0);
    workingEnd.setMilliseconds(0);

    // Get the totalDeptDayTime in seconds 9.5 hours
    const totalDeptDayTime = 34200;
    // Get the Ticket SLA in seconds then divide it by the days to see how much of a dept day it will take.
    const totalDeptDaysTaken = (
      (row.ticketSla * 60) /
      totalDeptDayTime
    ).toFixed(2);
    console.log("DEPT DAYS TAKEN", totalDeptDaysTaken);
    // Total amount of time taken in seconds REMEBER a dept day is 9.5 hours not 24 so we time by 9.5 to get the hours value.
    const totalDeptSecondsTaken = Math.round(
      totalDeptDaysTaken * 9.5 * 60 * 60
    );

    const normalizeDeptTime =
      parseInt(totalDeptDaysTaken[0] * 24) +
      parseInt(totalDeptDaysTaken[2] * 12) +
      parseInt(totalDeptDaysTaken[3] * 6);

    let completedBy = moment(row.fields.created).add(
      totalDeptSecondsTaken,
      "s"
    );
    // Difference from when it was completed to the current date.
    let difference = moment(completedBy).diff(moment(), "m");
    if (completedBy.unix() > moment(workingEnd).unix() && difference > 0) {
      let overBy = moment(completedBy).diff(workingEnd, "m");

      let diff = moment.duration(overBy, "m");
      let newDate = moment(workingStart)
        .add(1, "day")
        .add(diff);

      // workingEnd = moment(workingEnd).add(1, "day");

      // while (moment(newDate).unix() > moment(workingEnd).unix()) {
      //   overBy = moment(newDate).diff(workingEnd, "m");

      //   diff = moment.duration(overBy, "m");

      //   newDate = moment(workingStart)
      //     .add(1, "day")
      //     .add(diff);
      //   console.log(newDate, "THE NEW DATE");
      // }
      difference = moment(newDate).diff(moment(), "m");
    }

    Object.assign(row, {
      diff: difference,
      animationTime: difference * 6,
      humanizedResponse:
        moment.duration(difference, "m") < 0
          ? humanizeDuration(moment.duration(difference, "m")) + " ago"
          : "In " + humanizeDuration(moment.duration(difference, "m"))
    });
    updatedTicketList.push(row);
  });
  return { type: GET_TICKETSTIMETOSLA, jiraTickets: updatedTicketList };
}

export function updateConfetti(bool) {
  return { type: UPDATE_CONFETTI, showConfetti: bool };
}
