import {
  GET_JIRA_DATA,
  GET_TICKETS_SLAS,
  GET_TICKETSTIMETOSLA,
  ORDER_TICKETLIST,
  UPDATE_CONFETTI
} from "../constants";

const initialState = {
  jiraTickets: [],
  showConfetti: false
};

function rootReducer(state = initialState, action) {
  if (action.type === GET_JIRA_DATA) {
    return { ...state, jiraTickets: action.jiraTickets };
  }
  if (action.type === GET_TICKETS_SLAS) {
    return { ...state, jiraTickets: action.jiraTickets };
  }

  if (action.type === GET_TICKETSTIMETOSLA) {
    return { ...state, jiraTickets: action.jiraTickets };
  }
  if (action.type === ORDER_TICKETLIST) {
    return { ...state, jiraTickets: action.jiraTickets };
  }
  if (action.type === UPDATE_CONFETTI) {
    return { ...state, showConfetti: action.showConfetti };
  }

  return state;
}

export default rootReducer;
