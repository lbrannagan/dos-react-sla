import React, { useState, useEffect } from "react";
import styled, { keyframes } from "styled-components";
import moment from "moment";

const Td = styled.td`
  border: none;
  padding: 0.3em;
  background-color: ${props => {
    if (props.animationTime < 0) {
      return "black; color: white";
    } else if (props.animationTime < 1800) {
      return "red";
    } else if (props.animationTime < 7200) {
      return "orange";
    } else if (props.animationTime < 14400) {
      return "yellow";
    } else if (props.animationTime < 28800) {
      return "white";
    } else {
      return "white";
    }
  }};
`;
const Tr = styled.tr`
  &:nth-child(2n) {
    opacity: 0.85;
  }
`;
const Link = styled.a`
  text-decoration: none;
  color: unset;
  cursor: pointer;
`;

const TdHumanizedResponse = styled.td`
  border: none;
  padding: 0.3em;
  background-color: ${props => {
    if (props.animationTime < 0) {
      return "black; color: white";
    } else if (props.animationTime < 1800) {
      return "red";
    } else if (props.animationTime < 7200) {
      return "orange";
    } else if (props.animationTime < 14400) {
      return "yellow";
    } else if (props.animationTime < 28800) {
      return "white";
    } else {
      return "white";
    }
  }};
  max-width: 10em;
`;

const TableRow = ({
  ticketId,
  humanizedResponse,
  animationTime,
  fields: { summary, priority, assignee, created }
}) => {
  return (
    <Tr>
      <Td animationTime={animationTime}>
        {" "}
        <Link
          href={`http://supportdesk.building-blocks.com/browse/${ticketId}`}
        >
          {ticketId}{" "}
        </Link>
      </Td>
      <TdHumanizedResponse animationTime={animationTime}>
        {humanizedResponse}
      </TdHumanizedResponse>
      <Td animationTime={animationTime}>
        {summary.charAt(0).toUpperCase() + summary.slice(1).toLowerCase()}
      </Td>
      <Td animationTime={animationTime}>{priority.name}</Td>
      <Td animationTime={animationTime}>{assignee.displayName}</Td>
      <Td animationTime={animationTime}>
        {moment(created).format("MMMM Do YYYY LTS")}
      </Td>
    </Tr>
  );
};

export default TableRow;
