import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import TableRow from "./TableRow";

const TableContainer = styled.table`
  width: 100%;
  height: 100%;
  border: none;
  border-collapse: collapse;
`;
const Tr = styled.tr`
  &:nth-child(2n) {
    background-color: rgba(0, 0, 0, 0.1);
  }
`;
const Th = styled.th`
  padding-top: 1em;
`;
const Td = styled.td``;

const Table = ({ jiraTickets }) => {
  const table = useRef(null);
  const [headers, setHeaders] = useState([
    "Ticket Id",
    "Time until SLA",
    "Title",
    "Priority",
    "Assignee",
    "Created at"
  ]);

  return (
    <TableContainer ref={table}>
      <Tr>
        {headers && headers.map(header => <Th key={header}>{header}</Th>)}
      </Tr>
      {jiraTickets &&
        jiraTickets.map((ticket, index) => (
          <TableRow key={index} {...ticket} />
        ))}
    </TableContainer>
  );
};

export default Table;
