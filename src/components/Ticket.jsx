import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import moment from "moment";
import slaConfig from "../lib/config/sla.config";

const TicketLink = styled.a`
text-decoration: none ;
color: white;
&:link {
    text-decoration: none ;
}
&:visited {
    text-decoration: none ;
}

`;
const Container = styled.div`
margin: 1em;
background-color: #CAC8C9;
max-width: 50%;
min-width: 50%;
transition: 0.4s all;
border-radius: 5px;
position: relative;
&:hover {
    transform: scale(1.05);
}
&::before {
    content: "";
    height: 10%;
    width: 100%;
    background-color: ${props => {
    switch(props.slaLevel) {
  case "Blocker":
    return "red";
    break;
  case "Critical":
    return "red";
    break;
    case "Major":
    return "red";
    break;
    case "Normal":
    return "yellow"
    break;
    case "Minor":
    return "green"
    break;
    case "Trivial":
    return "blue"
    break;
    case "Nice to have":
    return "pink"
    break;
  default:
    
}
}};
    top: -0.3em;
    z-index: -1;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    position: absolute;
}

`;
const TicketTitle = styled.h3``;
const TicketContent = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    flex-direction: column;
    padding: 1em;
`;

const Assignee = styled.div``;
const Title = styled.div``;

const Created = styled.div``;
const Link = styled.a``;

const TimeUntilSLA = styled.p`
min-width: 15%;
height: 100%;
margin-right: 1em;
text-align: center;
padding:0.5em;
border-radius: 5px;
`;




const SlaContainer = styled.div``;
const TimeCreated = styled.p``;


const Ticket = ({fields, self, ticketId}) => {
  const [cardTimeTillSla, setCardTimeTillSla] = useState("");
  const [cardSlaLevel, setCardSlaLevel] = useState("");
  const [slaResponseTime, setSlaResponseTime] = useState("");
  
  function timeConvert(n) {
    const num = n;
    const hours = (num / 60);
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;
    const rminutes = Math.round(minutes);
    return rhours + " hour(s) and " + rminutes + " minute(s)";
    }


    const getSlas = () => {
        // Get Sla type and time. 
        let slaLevel = '';
        let slaTime = 0;
        slaConfig.forEach((sla) => {
            if(sla.level === fields.priority.name) {
                slaLevel = fields.priority.name;
                slaTime = sla.time;
            }
        })
        const createdTime = moment(fields.created);
        const slaMax = moment(createdTime).add(slaTime, "m");
        // Time Difference from current time in hours 
        const timeTillSla = moment().diff(slaMax, "m");
      
    
        setCardSlaLevel(slaLevel);
        setSlaResponseTime(slaTime);
        if(timeTillSla > 0 ) {
            setCardTimeTillSla("You have " + timeConvert(timeTillSla - slaTime) + " left to respond to the ticket.");
        } else {
            setCardTimeTillSla("You are " + timeConvert(timeTillSla) + " overdue to respond to this ticket.");
        }
        console.table(slaLevel ,timeTillSla, slaTime)
    }

useEffect(() => {
    getSlas();
}, [])

    return (
    <Container slaLevel={cardSlaLevel}>
        <TicketLink href={`http://supportdesk.building-blocks.com/browse/${ticketId}`}>
        <TicketContent>
            <TicketTitle>{fields.summary}</TicketTitle>
            <TimeUntilSLA >{cardSlaLevel + " - " + cardTimeTillSla + " left" + " to respond"}</TimeUntilSLA>
            <TimeCreated>Created: {moment(fields.created).fromNow()}</TimeCreated>
        </TicketContent>
        </TicketLink>
    </Container>
)
}

export default Ticket;