import React from "react";
import styled from "styled-components";

const Container = styled.div``;
const Input = styled.input``;
const Label = styled.label``;
const Button = styled.button``;
const Title = styled.h2``;

const LoginModal = () => {
  return (
    <Container>
      <Title>DOS SLA Tracker</Title>
      <Label>
        Username:
        <Input />
      </Label>
      <Label>
        Password:
        <Input type="password" />
      </Label>
      <Button>Login</Button>
    </Container>
  );
};

export default LoginModal;
