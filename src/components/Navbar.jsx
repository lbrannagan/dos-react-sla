import React from "react";
import styled from "styled-components";
import logo from "../assets/DEPT_LOGO.png";

const Container = styled.div`
  width: 99%;
  background-color: black;
  padding: 1em;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const Logo = styled.img`
  margin-left: 2em;
  height: 50%;
`;
const Content = styled.h3`
  color: white;
  margin-right: 2em;
  text-transform: uppercase;
`;

const Navbar = () => {
  return (
    <Container>
      <Logo src={logo} />
      <Content>DOS Jira Issue Tracker</Content>
    </Container>
  );
};

export default Navbar;
