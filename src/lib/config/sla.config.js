const slas = [{
  
    "level": "Blocker",
    "time": 30,
  },
  {
    "level": "Critical",
    "time": 240,
  },
  {
    "level": "Major",
    "time": 240,
  },
  {
    "level": "Normal",
    "time": 1440
  },
  {
    "level": "Minor",
    "time": 7200
  },
  {
    "level": "Trivial",
    "time": 7200
  },
  {
    "level": "Nice to have",
    "time": 7200
  }
]
  
        
        


export default slas;