// import moment from "moment";
// import rootReducer from "../redux/modules/reducers";
// import { useDispatch, useSelector } from "react-redux";
// import {
//   GET_JIRA_DATA,
//   GET_TICKETS_SLAS,
//   GET_TICKETSTIMETOSLA,
//   ORDER_TICKETLIST
// } from "../redux/modules/constants";
// import {
//   getJiraData,
//   getTicketsSlas,
//   getTimesToSla,
//   orderJiraTickets
// } from "../redux/modules/actions";

// let initalState = "";
// const dispatch = "";

// beforeEach(() => {
//   initalState = {
//     jiraTickets: []
//   };
//   dispatch = useDispatch();
// });
// describe("Table Tests.", () => {
//   const mockArray = [{ fields: { created: moment().subtract(3, "h") } }];

//   it("GET_TICKETS_SLAS - Should get the tickets sla and assign it a time.", () => {
//     const payload = {
//       type: GET_TICKETS_SLAS,
//       payload: [
//         { created: moment().subtract(3, "h"), ticketSla: 240 },
//         { created: moment().subtract(2, "h"), ticketSla: 240 },
//         { created: moment().subtract(5, "h"), ticketSla: 240 },
//         { created: moment().subtract(1, "h"), ticketSla: 240 }
//       ]
//     };
//     const ticketsWithSlas = dispatch(getTicketsSlas(payload.payload));
//     expect(ticketsWithSlas).toEqual().Array().length(2)
//   });
//   it("GET_TICKETSTIMETOSLA - Should get the tickets time till it hits its SLA, as well as the animation time and parse a human readable time.", () => {});
//   it("ORDER_TICKETLIST - Should order all the tickets based on date first then on time to SLA, time to SLA should take over date.", () => {});
// });

// describe("Animation Tests.", () => {
//   const mockArray = [{ fields: { created: moment().subtract(3, "h") } }];
//   it("should check the right colors are being assigned to the time until SLA.", () => {});
// });
