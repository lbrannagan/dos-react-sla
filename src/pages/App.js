import React, { useState, useEffect } from "react";
import styled from "styled-components";

import Navbar from "../components/Navbar";
import Confetti from "react-confetti";
import { useDispatch, useSelector } from "react-redux";
import {
  getJiraData,
  getTicketsSlas,
  getTimesToSla,
  orderJiraTickets,
  updateConfetti
} from "../redux/modules/actions";

import { createGlobalStyle } from "styled-components";

import Table from "../components/Table/Table";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: GoodProHeadline;
    src: url('../assets/fonts/FF Good - OTF/GoodHeadlinePro-XCondBlack.otf');
    font-style: normal;
    font-weight: 400;
  }
  body {
    margin: 0;
    padding: 0;
  }
  * {
    margin: 0;
    padding: 0;
    color: black;
    letter-spacing: 0.1px;
  }
  h1 {
    font-size: 40px;
    font-family: GoodProHeadline;
  }

  h2 {
    font-size: 32px;
  }
  h3 {
    font-family: GoodProHeadline;
  }

  p, tr, th, td {
    font-size: 20px;
    font-family: 'Martel Sans', sans-serif;
  }
  button {
    background: transparent;
  }
`;

const Container = styled.div`
  margin: 0;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 95%;
  margin: 0 auto;
`;

function App() {
  const dispatch = useDispatch();
  const { jiraTickets, showConfetti } = useSelector(state => state);
  useEffect(() => {
    if (jiraTickets.length <= 0) {
      dispatch(getJiraData());
    }
    setInterval(() => {
      dispatch(getJiraData());
    }, 60 * 1000);
  }, []);

  useEffect(() => {
    if (
      jiraTickets.length !== 0 &&
      parseInt(localStorage.getItem("ticketCount")) !== 0
    ) {
      if (parseInt(localStorage.getItem("ticketCount")) < jiraTickets.length) {
        dispatch(updateConfetti(true));
      }
    }

    localStorage.setItem("ticketCount", jiraTickets.length);
    if (jiraTickets.length !== 0 && !("ticketSla" in jiraTickets[0])) {
      dispatch(getTicketsSlas(jiraTickets));
      dispatch(getTimesToSla(jiraTickets));
      dispatch(orderJiraTickets(jiraTickets));
    }
    setTimeout(() => {
      dispatch(updateConfetti(false));
    }, 10 * 1000);
  }, [jiraTickets]);

  return (
    <>
      <GlobalStyle />
      <Navbar />
      <Container>
        {showConfetti ? (
          <Confetti width={window.innerWidth} height={window.innerHeight} />
        ) : null}
        {jiraTickets?.length > 0 && <Table jiraTickets={jiraTickets} />}
      </Container>
    </>
  );
}

export default App;
